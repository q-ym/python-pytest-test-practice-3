# -*- coding : utf-8 -*-
# @Author : …
# @Date : 2021/7/31 0:20
# @Project  File : pythonProject1  test_calc.fixture
import logging

import allure
import pytest
import yaml

from test_pytest.zuoye3.calculator import Calculator


def get_datas():
    with open('datas/calc.yml') as f :
        datas = yaml.safe_load(f)
    return datas


@allure.feature("计算器")
class TestCalc:
    def setup_class(self):
        logging.info("开始计算")
        self.calc = Calculator()

    def teardown_class(self):
        logging.info("结束计算")

    @allure.story("相加-正常情况")
    @pytest.mark.parametrize('a,b,expect',get_datas()['add-normal']['datas'],ids=get_datas()['add-normal']['ids'])
    def test_add_normal(self,a,b,expect):
        assert expect == self.calc.add(a,b)

    @allure.story("相加-浮点数的情况")
    @pytest.mark.parametrize('a,b,expect',get_datas()['add-float']['datas'],ids=get_datas()['add-float']['ids'])
    def test_add_float(self,a,b,expect):
        assert expect == round(self.calc.add(a,b),4)

    @allure.story("相加-异常情况")
    @pytest.mark.parametrize('a,b',get_datas()['add-error']['datas'],ids=get_datas()['add-error']['ids'])
    def test_add_error(self,a,b):
        with pytest.raises(TypeError):
            result = self.calc.add(a,b)

    @allure.story("相除-正常情况")
    @pytest.mark.parametrize('a,b,expect',get_datas()['div-normal']['datas'],ids=get_datas()['div-normal']['ids'])
    def test_div_normal(self,a,b,expect):
        assert expect == self.calc.div(a,b)

    @allure.story("相除-浮点数的情况")
    @pytest.mark.parametrize('a,b,expect',get_datas()['div-float']['datas'],ids=get_datas()['div-float']['ids'])
    def test_div_float(self,a,b,expect):
        # 加round限制保留4位小数
        assert expect == round(self.calc.div(a,b),4)

    @allure.story("相除-异常情况")
    @pytest.mark.parametrize('a,b',get_datas()['div-error']['datas'],ids=get_datas()['div-error']['ids'])
    def test_div_error(self,a,b):
        # 异常情况：除数为0的情况
        # 预期异常：不抛异常是错的，抛异常才是正常的情况
        with pytest.raises(ZeroDivisionError):
            result = self.calc.div(a,b)
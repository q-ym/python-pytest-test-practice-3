# -*- coding : utf-8 -*-
# @Author : …
# @Date : 2021/7/31 0:21
# @Project  File : pythonProject1  conftest
import logging

import pytest

from test_pytest.zuoye3.calculator import Calculator

# fixture获取计算器的实例
@pytest.fixture(scope='class')
def get_calc_object():
    # setup
    logging.info("开始计算")
    calc = Calculator()
    yield calc
    # teardown
    logging.info("结束计算")